from rest_framework import serializers
from api.models import Article

# Article Serializer
class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ('name', 'content', 'created_at')